(ns shouter.models.migration
  (:require [clojure.java.jdbc :as sql]))

(def db-spec "mysql://root:abc123@localhost:3306/test")

(defn create-shouts []
  (sql/with-connection db-spec
                       (sql/create-table :shouts
                                         [:id :integer "PRIMARY KEY" "AUTO_INCREMENT"]
                                         [:body "varchar(255)" "NOT NULL"]
                                         [:created_at :timestamp "NOT NULL" "DEFAULT CURRENT_TIMESTAMP"])))

(defn -main []
  (println "Migration database....") (flush)
  (create-shouts)
  (println "done"))
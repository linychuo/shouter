(ns shouter.models.shout
  (:require [clojure.java.jdbc :as sql]))

(def db-spec "mysql://root:abc123@localhost:3306/test")

(defn all []
  (sql/with-connection db-spec
                       (sql/with-query-results results
                                               ["select * from shouts order by id desc"]
                                               (into [] results))))

(defn create [shout]
  (sql/with-connection db-spec
                       (sql/insert-values :shouts [:body] [shout])))
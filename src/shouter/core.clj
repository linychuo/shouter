(ns shouter.core
  (:use [compojure.core :only [defroutes]])
  (:require [compojure.route :as route]
            [compojure.handler :as handler]
            [shouter.controllers.shouts]
            [shouter.views.layout :as layout]))

(defroutes routes
           shouter.controllers.shouts/routes
           (route/resources "/")
           (route/not-found (layout/four-oh-four)))

(def application (handler/site routes))
(ns shouter.views.shouts
  (:use [hiccup.core :only [h]]
        [hiccup.form :only [form-to label text-field submit-button]])
  (:require [shouter.views.layout :as layout]))

(defn shout-form []
  [:div {:id "shout-form"}
   (form-to [:post "/"]
            (label "shout" "来吧，喊!")
            (text-field "shout")
            (submit-button "叫!"))])

(defn display-shouts [shouts]
  [:div {:id "shouts sixteen columns alpha omega"}
   (map (fn [shout] [:h2 (h (:body shout))]) shouts)])

(defn index [shouts]
  (layout/common "大喊"
                 (shout-form)
                 (display-shouts shouts)))
(ns shouter.views.layout
  (:use [hiccup.page :only [html5]]))

(defn common [title & body]
  (html5
    [:head
     [:meta {:http-equiv "Content-Type" :content "text/html; charset=utf-8"}]
     [:title title]]
    [:body
     [:div {:id "header"}
      [:h1 "大喊"]]
     [:div {:id "content"} body]]))

(defn four-oh-four []
  (common "Page Not Found"
          [:div {:id "four-oh-four"}
           "The page you requested could not be found"]))
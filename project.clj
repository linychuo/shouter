(defproject shouter "0.1.0-SNAPSHOT"
            :description "FIXME: write description"
            :url "http://example.com/FIXME"
            :dependencies [[org.clojure/clojure "1.6.0"]
                           [mysql/mysql-connector-java "5.1.6"]
                           [org.clojure/java.jdbc "0.2.2"]
                           [compojure "1.1.8"]
                           [hiccup "1.0.0"]]
            :plugins [[lein-ring "0.8.11"]]
            :ring {:handler shouter.core/application})
